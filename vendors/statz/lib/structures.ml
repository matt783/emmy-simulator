module type Ordered =
sig
  type t
  val compare : t -> t -> int
end

module type Metric =
sig
  type t
  val dist : t -> t -> float
end

module type Linear =
sig
  type t
  val zero : t
  val (+) : t -> t -> t
  val ( * ) : float -> t -> t
end

module Int : sig
  type t = int
  include Ordered with type t := int
end = struct
  type t = int
  let compare (i : int) (j : int) =
    if i < j then -1
    else if i > j then 1
    else 0
end

module Bool : sig
  type t = bool
  include Ordered with type t := bool
end = struct
  type t = bool
  let compare (i : bool) (j : bool) =
    match i, j with
    | false, false
    | true, true -> 0
    | false, true -> -1
    | true, false -> 1
end

module Float : sig
  type t = float
  include Ordered with type t := float
  include Linear with type t := float
end = struct
  type t = float
  let compare = Float.compare
  let zero = 0.0
  let (+) = (+.)
  let ( * ) = ( *. )
end
