#!/bin/bash

WORKING_DIR=`pwd`
VENV_DIR=${WORKING_DIR}/venv

echo "Currently in $WORKING_DIR."
echo "Installing Python virtualenv in $VENV_DIR."

function packages_install () {
    echo "Sourcing ${VENV_DIR}/bin/activate"
    source ${VENV_DIR}/bin/activate
    if [ $? -eq 0 ]; then
        echo "Source succeeded."
    else
        echo "Source failed."
        exit 1
    fi
    pip3 install matplotlib
}

if [ -d ${VENV_DIR} ]; then
    echo "Directory already exists. Proceeding to package installation."
    packages_install
    exit 0
else
    echo "Creating virtualenv..."
    python3 -m venv ${VENV_DIR}
    if [ $? -eq 0 ]; then
        echo "Success."
    else
        exit 1
    fi
    packages_install
fi
