(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Nomadic Labs. <nomadic@tezcore.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(*
   The Baking game
   ---------------
   N players. The players play synchronously.
   Each player has a two-phase strategy:
   - Endorsement phase: looking at the [level], decide how many endorsements
     to reveal
   - Baking phase: looking at the [mempool], decide whether to bake or not,
     and if so, decide how many public endorsements to include.

   There are at least two possible objectives to the game
   - to maximize one's reward in absolute terms
   - to maximize one's reward _relatively_ to the other players

   Strategies are probabilistic. Since several players
   might decide cocurrently to bake, the outcome of the game
   is non-deterministic _and_ probabilistic. To be precise,
   if ND is the nondeterminism monad, the outcome of a
   round is in ND.t M.t.

   The semantics of having several players issuing a block is the following:
   we consider the state of the world to be the block tree. Each node in
   this tree is a potentially valid head. Each node has an _age_. Heads
   which age are above a certain threshold above the youngest head at the
   same level are automatically culled by the simulation engine.
*)

module type S = functor
  (P : Player.S)
  (H : Player.Helpers_S with type Player_map.key = P.t)
  -> sig
  open H

  type player = P.t

  type level = {block_priority : priorities; endorsers : endorsers}

  and priorities = Int_vec.t

  and endorsers = Int_vec.t

  type levels = int -> level

  type block = {
    age : float;
    fitness : int;
    rewards : Float_vec.t;
    level : level;
    outcome : outcome;
    block_uid : Block_id.t;
  }

  and outcome = {
    delay : float;
    baker : player;
    slot : int;
    block_reward : float;
    endorsement_rewards : Float_vec.t;
    prev_uid : Block_id.t;
  }

  type heads = block list

  type endorsement_move = Diffuse_endorsements of (int * Block_id.t) option

  type baking_move =
    | Bake of {used_public_endorsements : endorsers; on_top_of : Block_id.t}
    | Do_not_bake

  type player_mempool = {
    published_endorsements : (int * Block_id.t) option;
    secret_endorsements : int;
  }

  type mempool = {
    public_endorsements : Int_vec.t Block_id.Map.t;
    mempools : player_mempool Player_map.t;
  }

  (* Strategies, arenas. *)
  type arena = strategy Player_map.t

  and strategy =
    | Strategy of
        (height:int ->
        levels:levels ->
        heads:heads ->
        endorsement_strategy M.t)

  and endorsement_strategy = endorsement_move * baking_strategy

  and baking_strategy = mempool -> (baking_move * strategy) M.t

  val pp_mempool : Format.formatter -> player_mempool Player_map.t -> unit

  val pp_outcome : Format.formatter -> outcome -> unit

  val pp_block : Format.formatter -> block -> unit

  val pp_level : Format.formatter -> level -> unit

  val level_encoding : level Data_encoding.t

  val outcome_encoding : outcome Data_encoding.t

  val block_encoding : block Data_encoding.t

  val genesis_level : level

  val genesis_outcome : outcome

  val bake : int -> levels -> heads -> arena -> (outcome list * arena) M.t
end

module Make (Proto : Protocol.S) : S =
functor
  (P : Player.S)
  (H : Player.Helpers_S with type Player_map.key = P.t)
  ->
  struct
    open H

    type player = P.t

    type level = {block_priority : priorities; endorsers : endorsers}

    and priorities = Int_vec.t

    and endorsers = Int_vec.t

    type levels = int -> level

    type block = {
      age : float;
      fitness : int;
      rewards : Float_vec.t;
      (* _total_ rewards *)
      level : level;
      outcome : outcome;
      block_uid : Block_id.t;
    }

    and outcome = {
      delay : float;
      baker : player;
      slot : int;
      block_reward : float;
      (* block baking reward *)
      endorsement_rewards : Float_vec.t;
      (* block endo rewards *)
      prev_uid : Block_id.t;
    }

    type heads = block list

    (* Moves and strategies *)

    (* Moves *)
    type endorsement_move = Diffuse_endorsements of (int * Block_id.t) option

    (* When a baker bakes, it does so with all its secret endorsements
     and a specified amount of public ones. *)
    type baking_move =
      | Bake of {used_public_endorsements : endorsers; on_top_of : Block_id.t}
      | Do_not_bake

    type player_mempool = {
      published_endorsements : (int * Block_id.t) option;
      secret_endorsements : int;
    }

    type mempool = {
      public_endorsements : Int_vec.t Block_id.Map.t;
      mempools : player_mempool Player_map.t;
    }

    type arena = strategy Player_map.t

    and strategy =
      | Strategy of
          (height:int ->
          levels:levels ->
          heads:heads ->
          endorsement_strategy M.t)

    and endorsement_strategy = endorsement_move * baking_strategy

    and baking_strategy = mempool -> (baking_move * strategy) M.t

    let () =
      (* Assert health values make sense *)
      for i = 0 to Array.length P.all - 1 do
        let h = P.health P.all.(i) in
        assert (0. <= h && h <= 1.)
      done

    (* let pp_published_endorsements fmtr (map : int Block_id.Map.t) =
     *   let bindings = Block_id.Map.bindings map in
     *   Format.pp_print_list
     *     (fun fmtr (block_id, endo_count) ->
     *       Format.fprintf
     *         fmtr
     *         "endorse %a with %d endos"
     *         Block_id.pp
     *         block_id
     *         endo_count)
     *     fmtr
     *     bindings *)

    let pp_mempool fmtr mempool =
      let pp_player_mempool fmtr
          {published_endorsements = opt; secret_endorsements} =
        match opt with
        | Some (count, bl_id) ->
            Format.fprintf
              fmtr
              "@[{published=%d @ %a;@,secret=%d}@]"
              count
              Block_id.pp
              bl_id
              secret_endorsements
        | None ->
            Format.fprintf
              fmtr
              "@[{published= None;@,secret=%d}@]"
              secret_endorsements
      in
      let bindings = Player_map.bindings mempool in
      Format.pp_print_list
        (fun fmtr (player, pmp) ->
          Format.fprintf fmtr "%a |-> %a" P.pp player pp_player_mempool pmp)
        fmtr
        bindings

    let pp_outcome fmtr (outcome : outcome) =
      Format.fprintf
        fmtr
        "@[{delay=%f;@,\
         baker=%a;@,\
         slot=%d;@,\
         block_reward=%f;@,\
         rewards=@,\
         %a;@,\
         prev=%a}@]"
        outcome.delay
        P.pp
        outcome.baker
        outcome.slot
        outcome.block_reward
        Float_vec.pp
        outcome.endorsement_rewards
        Block_id.pp
        outcome.prev_uid

    let pp_int_player_map fmtr map = Int_vec.pp fmtr map

    let pp_level fmtr {block_priority; endorsers} =
      Format.fprintf
        fmtr
        "@[{block_priority=%a;@;endorsers=%a@,}@]"
        pp_int_player_map
        block_priority
        pp_int_player_map
        endorsers

    let pp_block fmtr block =
      Format.fprintf
        fmtr
        "@[{age=%f;@;\
         fitness=%d;@;\
         rewards=%a;@;\
         level=%a@;\
         outcome=%a@;\
         block_uid=%a}@]"
        block.age
        block.fitness
        Float_vec.pp
        block.rewards
        pp_level
        block.level
        pp_outcome
        block.outcome
        Block_id.pp
        block.block_uid

    let level_encoding : level Data_encoding.t =
      let open Data_encoding in
      conv
        (fun {block_priority; endorsers} -> (block_priority, endorsers))
        (fun (block_priority, endorsers) -> {block_priority; endorsers})
        (tup2 Int_vec.encoding Int_vec.encoding)

    let outcome_encoding : outcome Data_encoding.t =
      let open Data_encoding in
      conv
        (fun {delay; baker; slot; block_reward; endorsement_rewards; prev_uid} ->
          (delay, baker, slot, block_reward, endorsement_rewards, prev_uid))
        (fun (delay, baker, slot, block_reward, endorsement_rewards, prev_uid) ->
          {delay; baker; slot; block_reward; endorsement_rewards; prev_uid})
        (tup6 float P.encoding int31 float Float_vec.encoding Block_id.encoding)

    let block_encoding : block Data_encoding.t =
      let open Data_encoding in
      conv
        (fun {age; fitness; rewards; level; outcome; block_uid} ->
          (age, fitness, rewards, level, outcome, block_uid))
        (fun (age, fitness, rewards, level, outcome, block_uid) ->
          {age; fitness; rewards; level; outcome; block_uid})
        (tup6
           float
           int31
           Float_vec.encoding
           level_encoding
           outcome_encoding
           Block_id.encoding)

    let genesis_level =
      {block_priority = Int_vec.init ~-1; endorsers = Int_vec.init ~-1}

    let genesis_outcome =
      {
        delay = 0.0;
        baker = P.all.(0);
        slot = 0;
        block_reward = 0.0;
        endorsement_rewards = Float_vec.zero;
        prev_uid = Block_id.fresh ();
      }

    (* ----------------------------------------------------------------------- *)
    (* The rules of the game *)

    (* let total_count : int Block_id.Map.t -> int =
     *  fun map -> Block_id.Map.fold (fun _ x acc -> acc + x) map 0 *)

    let bake_generic :
        baker:player ->
        priority:int ->
        used_public_endorsements:endorsers ->
        mempool:mempool ->
        on_top_of:Block_id.t ->
        outcome =
     fun ~baker ~priority ~used_public_endorsements ~mempool ~on_top_of ->
      let public_endorsements_count = Int_vec.total used_public_endorsements in
      let baker_mempool =
        try Player_map.find baker mempool.mempools
        with Not_found -> assert false
      in
      let endorsements =
        baker_mempool.secret_endorsements + public_endorsements_count
      in
      let delay =
        Proto.minimal_valid_time ~priority ~endorsing_power:endorsements
      in
      (* Compute the block reward *)
      let block_reward =
        Proto.block_reward ~priority ~included_endorsements:endorsements
      in
      (* Compute the endorsement rewards for each player *)
      let endorsement_rewards =
        Array.fold_left
          (fun acc player ->
            let retained_endorsements =
              Int_vec.get player used_public_endorsements
            in
            let num_slots =
              if P.compare player baker = 0 then
                retained_endorsements + baker_mempool.secret_endorsements
              else retained_endorsements
            in
            let rewards = Proto.endorsing_reward ~priority ~num_slots in
            Float_vec.(add acc (basis player rewards)))
          Float_vec.zero
          P.all
      in
      {
        delay;
        baker;
        slot = priority;
        block_reward;
        endorsement_rewards;
        prev_uid = on_top_of;
      }

    let multiset_sum :
        Int_vec.t Block_id.Map.t ->
        Int_vec.t Block_id.Map.t ->
        Int_vec.t Block_id.Map.t =
     fun map1 map2 ->
      Block_id.Map.union
        (fun _k map1 map2 -> Some (Int_vec.add map1 map2))
        map1
        map2

    let lift_player_endorsement_strat :
        int -> Block_id.t -> P.t -> Int_vec.t Block_id.Map.t =
     fun count bl p -> Block_id.Map.singleton bl (Int_vec.basis p count)

    let bake : int -> levels -> heads -> arena -> (outcome list * arena) M.t =
     fun height levels heads arena ->
      let level = levels height in
      let open M in
      let public_endorsements =
        List.fold_left
          (fun map {block_uid; _} ->
            Block_id.Map.add block_uid Int_vec.zero map)
          Block_id.Map.empty
          heads
      in
      Player_map.fold_m
        (fun player
             (Strategy strategy : strategy)
             (public, secret_map, baking_strat_map) ->
          strategy ~height ~levels ~heads
          >>= fun endorsement_strat ->
          let (endo_move, baking_strat) = endorsement_strat in
          let (Diffuse_endorsements diffuse_opt) = endo_move in
          let available_endorsements = Int_vec.get player level.endorsers in
          let n =
            match diffuse_opt with None -> 0 | Some (n, _block_id) -> n
          in
          assert (available_endorsements >= n) ;
          let secret =
            {
              published_endorsements = diffuse_opt;
              secret_endorsements = available_endorsements - n;
            }
          in
          let secret_map = Player_map.add player secret secret_map in
          let baking_strat_map =
            Player_map.add player baking_strat baking_strat_map
          in
          let public =
            match diffuse_opt with
            | None ->
                public
            | Some (n, block_id) ->
                let published_endorsements =
                  lift_player_endorsement_strat n block_id player
                in
                multiset_sum public published_endorsements
          in
          return (public, secret_map, baking_strat_map))
        arena
        (public_endorsements, Player_map.empty, Player_map.empty)
      >>= fun (public_endorsements, mempools, baking_strats) ->
      let mempool = {public_endorsements; mempools} in
      (* Format.eprintf "mempool:@.%a@." pp_mempool mempool.mempools ; *)
      Player_map.fold_m
        (fun player baking_strat (outcome_acc, arena) ->
          let open M in
          baking_strat mempool
          >>= fun (baking_move, new_strategy) ->
          ( match baking_move with
          | Bake {used_public_endorsements; on_top_of} ->
              let priority = Int_vec.get player level.block_priority in
              let available_public_endorsements =
                match Block_id.Map.find_opt on_top_of public_endorsements with
                | None ->
                    assert false
                | Some public_endos ->
                    public_endos
              in
              assert (
                Int_vec.le
                  used_public_endorsements
                  available_public_endorsements ) ;
              let outcome =
                bake_generic
                  ~baker:player
                  ~priority
                  ~used_public_endorsements
                  ~mempool
                  ~on_top_of
              in
              return (outcome :: outcome_acc)
          | Do_not_bake ->
              return outcome_acc )
          >>= fun outcome_acc ->
          let arena = Player_map.add player new_strategy arena in
          return (outcome_acc, arena))
        baking_strats
        ([], Player_map.empty)
  end
