(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Nomadic Labs. <nomadic@tezcore.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* Vectors implemented as maps. *)

module type Arith_sig = sig
  type t

  val zero : t

  val one : t

  val ( + ) : t -> t -> t

  val ( - ) : t -> t -> t

  val ( * ) : t -> t -> t

  val ( = ) : t -> t -> bool

  val ( <= ) : t -> t -> bool

  val encoding : t Data_encoding.t

  val pp : Format.formatter -> t -> unit
end

module type Key_sig = sig
  type t

  val encoding : t Data_encoding.encoding

  val pp : Format.formatter -> t -> unit

  val all : t array
end

module type S = sig
  type key

  type value

  type t

  type raw

  val zero : t

  val init : value -> t

  val basis : key -> value -> t

  (** Returns zero by default *)
  val get : key -> t -> value

  val add : t -> t -> t

  val sub : t -> t -> t

  val smul : value -> t -> t

  val le : t -> t -> bool

  val equal : t -> t -> bool

  val map : (value -> value) -> t -> t

  val total : t -> value

  val encoding : t Data_encoding.t

  val pp : Format.formatter -> t -> unit

  val raw : t -> raw

  module Ops : sig
    val ( + ) : t -> t -> t

    val ( - ) : t -> t -> t

    val ( *| ) : value -> t -> t

    val ( <= ) : t -> t -> bool

    val ( = ) : t -> t -> bool
  end
end

module Make (K : Key_sig) (V : Arith_sig) (M : Map.S with type key = K.t) :
  S with type key = K.t and type value = V.t and type raw = V.t M.t = struct
  type key = K.t

  type value = V.t

  type t = value M.t

  type raw = V.t M.t

  let pp fmtr map =
    let bindings = M.bindings map in
    Format.open_hbox () ;
    Format.pp_print_list
      ~pp_sep:(fun fmtr () -> Format.fprintf fmtr "; ")
      (fun fmtr (k, v) -> Format.fprintf fmtr "%a |-> %a" K.pp k V.pp v)
      fmtr
      bindings ;
    Format.close_box ()

  let add : t -> t -> t =
   fun v1 v2 -> M.union (fun _k x1 x2 -> Some V.(x1 + x2)) v1 v2

  let sub : t -> t -> t =
   fun v1 v2 -> M.union (fun _k x1 x2 -> Some V.(x1 - x2)) v1 v2

  let smul : value -> t -> t = fun s v -> M.map (fun x -> V.(x * s)) v

  let zero = Array.fold_left (fun acc p -> M.add p V.zero acc) M.empty K.all

  let init i = Array.fold_left (fun acc p -> M.add p i acc) M.empty K.all

  let basis : M.key -> value -> t = fun k x -> add zero (M.singleton k x)

  let get k v =
    try M.find k v
    with Not_found ->
      let msg =
        Format.asprintf "Vec: error accessing key %a in vec %a@." K.pp k pp v
      in
      invalid_arg msg

  let le : t -> t -> bool =
   fun v1 v2 ->
    M.fold
      (fun key x acc ->
        match M.find_opt key v2 with
        | None ->
            V.(x = zero)
        | Some y ->
            acc && V.(x <= y))
      v1
      true

  let equal x y = le x y && le y x

  let total v = M.fold (fun _ x acc -> V.(x + acc)) v V.zero

  let raw x = x

  let map : (value -> value) -> t -> t = M.map

  let encoding =
    let open Data_encoding in
    conv
      M.bindings
      (fun bindings -> M.of_seq (List.to_seq bindings))
      (list (tup2 K.encoding V.encoding))

  module Ops = struct
    let ( + ) = add

    let ( - ) = sub

    let ( *| ) = smul

    let ( <= ) = le

    let ( = ) = equal
  end
end

module Int (K : Key_sig) (X : Map.S with type key = K.t) =
  Make
    (K)
    (struct
      type t = int

      let zero = 0

      let one = 1

      let ( + ) = ( + )

      let ( - ) = ( - )

      let ( * ) = ( * )

      let ( = ) = ( = )

      let ( <= ) = ( <= )

      let encoding = Data_encoding.int31

      let pp = Format.pp_print_int
    end)
    (X)

module Float (K : Key_sig) (X : Map.S with type key = K.t) =
  Make
    (K)
    (struct
      type t = float

      let zero = 0.

      let one = 1.

      let ( + ) = ( +. )

      let ( - ) = ( -. )

      let ( * ) = ( *. )

      let ( = ) = ( = )

      let ( <= ) = ( <= )

      let encoding = Data_encoding.float

      let pp = Format.pp_print_float
    end)
    (X)
