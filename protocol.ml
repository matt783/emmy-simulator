(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Nomadic Labs. <nomadic@tezcore.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module type S = sig
  type proto_parameters

  val name : string

  val pparams : proto_parameters

  val endorsers_per_block : int

  val total_rewards_per_block : float

  val minimal_valid_time : priority:int -> endorsing_power:int -> float

  (* Assumption: reward is monotonically increasing in
     [included_endorsements]*)
  val block_reward : priority:int -> included_endorsements:int -> float

  (* Assumption: reward is monotonically increasing in [num_slots] *)
  val endorsing_reward : priority:int -> num_slots:int -> float
end

(* Functions shared across several protocols *)

type emmy_B_parameters = {
  time_between_blocks : float array;
  delay_per_missing_endorsement : float;
  endorsers_per_block : int;
  initial_endorsers : int;
  block_reward : float;
  endorsement_reward : float;
}

let default_emmy_B_parameters =
  {
    time_between_blocks = [|60.0; 40.0|];
    delay_per_missing_endorsement = 8.0;
    endorsers_per_block = 32;
    initial_endorsers = 24;
    block_reward = 16.;
    endorsement_reward = 2.;
  }

let emmy_plus :
    emmy_B_parameters ->
    (module S with type proto_parameters = emmy_B_parameters) =
 fun pparams ->
  ( module struct
    type proto_parameters = emmy_B_parameters

    let name = "emmy+B"

    let pparams = pparams

    let endorsers_per_block = pparams.endorsers_per_block

    let total_rewards_per_block =
      pparams.block_reward
      +. (float pparams.endorsers_per_block *. pparams.endorsement_reward)

    (* minimal age for block baked at priority
     [priority] with [included_endorsements] endorsements *)
    let minimal_valid_time ~priority ~endorsing_power =
      pparams.time_between_blocks.(0)
      +. (float priority *. pparams.time_between_blocks.(1))
      +. pparams.delay_per_missing_endorsement
         *. max 0.0 (float (pparams.initial_endorsers - endorsing_power))

    (* (\* All computations are done in floating point: we might get sub-mutez
     *  values creeping in. I'm assuming it doesn't matter for the analysis. *\)
     * let _block_reward ~priority ~included_endorsements =
     *   assert (priority >= 0) ;
     *   let max_endorsements = pparams.endorsers_per_block in
     *   assert (
     *     included_endorsements >= 0 && included_endorsements <= max_endorsements
     *   ) ;
     *   let block_reward = 1_000_000.0 *. pparams.block_reward in
     *   (\* in mutez *\)
     *   let numerator =
     *     block_reward
     *     *. ( (80. *. float max_endorsements)
     *        +. (20. *. float included_endorsements) )
     *   in
     *   let denominator =
     *     (float priority +. 1.) *. (100.0 *. float max_endorsements)
     *   in
     *   let mutez_result = numerator /. denominator in
     *   let tez_result = mutez_result /. 1_000_000.0 in
     *   tez_result *)

    let block_reward ~priority ~included_endorsements =
      let e_ratio =
        float included_endorsements /. float pparams.endorsers_per_block
      in
      pparams.block_reward
      /. (float priority +. 1.)
      *. (0.8 +. (0.2 *. e_ratio))

    (** Endorsement rewards for a delegate that has [num_slots] in a block of [priority] *)
    let endorsing_reward ~priority ~num_slots =
      assert (priority >= 0) ;
      let endorsement_reward = pparams.endorsement_reward in
      let denominator = float priority +. 1. in
      let numerator = endorsement_reward *. float num_slots in
      numerator /. denominator
  end )

type emmy_C_parameters = {
  time_between_blocks : float array;
  delay_per_missing_endorsement : float;
  endorsers_per_block : int;
  initial_endorsers : int;
  total_reward : float;
  baker_share : float;
  b : float;
}

let default_emmy_C_parameters =
  {
    time_between_blocks = [|60.0; 40.0|];
    delay_per_missing_endorsement = 8.0;
    endorsers_per_block = 32;
    initial_endorsers = 24;
    total_reward = 80.;
    baker_share = 0.5;
    b = 1.5;
  }

let chambart_plus pparams =
  if pparams.baker_share < 0.0 || pparams.baker_share > 1.0 then
    failwith "chambart_plus: invalid baker_share"
  else
    let module Chambart = struct
      type proto_parameters = emmy_C_parameters

      let name = "emmy+C"

      let pparams = pparams

      let endorsers_per_block = pparams.endorsers_per_block

      let total_rewards_per_block = pparams.total_reward

      let minimal_valid_time ~priority ~endorsing_power =
        pparams.time_between_blocks.(0)
        +. (float priority *. pparams.time_between_blocks.(1))
        +. pparams.delay_per_missing_endorsement
           *. max 0.0 (float (pparams.initial_endorsers - endorsing_power))

      let block_reward ~priority ~included_endorsements =
        match priority with
        | 0 ->
            float included_endorsements
            /. float endorsers_per_block *. pparams.total_reward
            *. pparams.baker_share
        | _ ->
            float included_endorsements /. float endorsers_per_block *. 6.

      let endorsing_reward ~priority ~num_slots =
        match priority with
        | 0 ->
            float num_slots /. float endorsers_per_block
            *. pparams.total_reward
            *. (1. -. pparams.baker_share)
        | _ ->
            float num_slots /. float endorsers_per_block
            *. pparams.total_reward
            *. (1. -. pparams.baker_share)
            *. 1. /. pparams.b
    end in
    (module Chambart : S with type proto_parameters = emmy_C_parameters)

let chambart_minus pparams =
  if pparams.baker_share < 0.0 || pparams.baker_share > 1.0 then
    failwith "chambart_minus: invalid parameter r"
  else
    let module Chambart = struct
      include (val chambart_plus pparams)

      let name = "emmy-C"

      let block_reward ~priority ~included_endorsements =
        match priority with
        | 0 ->
            float included_endorsements
            /. float endorsers_per_block *. pparams.total_reward
            *. pparams.baker_share
        | _ ->
            float included_endorsements /. float endorsers_per_block *. 6.

      let endorsing_reward ~priority ~num_slots =
        match priority with
        | 0 ->
            float num_slots /. float endorsers_per_block
            *. pparams.total_reward
            *. (1. -. pparams.baker_share)
        | _ ->
            float num_slots /. float endorsers_per_block
            *. pparams.total_reward
            *. (1. -. pparams.baker_share)
            *. 1. /. pparams.b
    end in
    (module Chambart : S with type proto_parameters = emmy_C_parameters)
