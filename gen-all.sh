#!/bin/sh

rm *.trace

make

for A in `./simu list adversaries`; do
    for P in `./simu list protocols`; do
        ./simu sample -a $A -p $P -s 8987 -n 3000
    done
done

./simu compute AR on *.trace
./simu compute IAR on *.trace
./simu compute RSD on *.trace
