(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Nomadic Labs. <nomadic@tezcore.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* Module type describing the players in a game *)
module type S = sig
  type t

  (** Players are totally ordered *)
  val compare : t -> t -> int

  (** Players can be serialized *)
  val encoding : t Data_encoding.t

  (** Players can be pretty-printed *)
  val pp : Format.formatter -> t -> unit

  (** There is a finite list of players, stored in [all] *)
  val all : t array

  (* \forall p. 0 <= health p <= 1 *)
  val health : t -> float

  (* \sum_{p \in players} share p <= 1 (b.c. of undelegated accounts) *)
  val share : t -> float
end

module type Helpers_S = sig
  module Player_map : M.Map_S

  module Float_vec :
    Vec.S with type key = Player_map.key and type value = float

  module Int_vec : Vec.S with type key = Player_map.key and type value = int
end

module Make_helpers (P : S) : Helpers_S with type Player_map.key = P.t = struct
  module Player_map = M.Make_map (P)
  module Float_vec = Vec.Float (P) (Player_map)
  module Int_vec = Vec.Int (P) (Player_map)
end
